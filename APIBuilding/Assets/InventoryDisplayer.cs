﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryDisplayer : MonoBehaviour
{
    public Inventory InventoryToDisplay;

    public Text SlotCount;
    public Text WarriorAmountDisplayer;
    public Text RejuvenatingPotionDisplayer;
    public Text AntidotePotionDisplayer;
    public Text WeirdMushroomDisplayer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SlotCount.text = "Slots:" + InventoryToDisplay.Items.Count.ToString() + "/" + InventoryToDisplay.MaxSlotCount.ToString();
        WarriorAmountDisplayer.text = InventoryToDisplay.GetQuantity<WarriorEmblem>().ToString();
        RejuvenatingPotionDisplayer.text = InventoryToDisplay.GetQuantity<RejuvenationPotion>().ToString();
        AntidotePotionDisplayer.text = InventoryToDisplay.GetQuantity<Antidote>().ToString();
        WeirdMushroomDisplayer.text = InventoryToDisplay.GetQuantity<WeirdMushroom>().ToString();

    }
}
