﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntidoteEffect : MonoBehaviour
{
    public WMEffect WMEffectObject;

    public void Effect()
    {
        WMEffectObject.isPoisoned = false;
    }
}
