﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BuffActivated : UnityEvent<Buff> { }
public class BuffDeactivated : UnityEvent<Buff> { }

public class Buff : MonoBehaviour
{
    public BuffActivated Activated = new BuffActivated();
    public BuffDeactivated Deactivated = new BuffDeactivated();

    public BuffReceiver Target { get; private set; }
    public float Duration;


    public bool IsDurInfinite;
    public bool IsStackByIntensity;

    public int StackCount { get; set; }

    public virtual void Activate(BuffReceiver target)
    {
        Target = target;
        Activated.Invoke(this);
    }

    protected virtual void Update()
    {
       // Debug.Log("Timer: " + Duration + " Stack: " + StackCount);
        if (StackCount >= 1)
        {
            if (Duration >= 1)
            {
                Duration -= 1 * Time.deltaTime;

                if (Duration <= 1)
                {
                    StackCount -= 1; // Removes a stack if timer reaches 0.
                    Duration = 0;
                  //Destroy(gameObject, Delay);
                }
            }
        }
    }

    public virtual void Deactivate()
    {
        Deactivated.Invoke(this);
    }
}
