﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Died : UnityEvent<Health> { }

public class HPChanged : UnityEvent<Health, float> { }

public class Health : MonoBehaviour
{
    public float MaxHP;
    public float CurrentHP;

    public HPChanged HpChanged = new HPChanged();
    public Died Died = new Died();

    Character character;

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<Character>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Health: " + CurrentHP);
    }

    public void AddHealth(float health)
    {
        float prev = CurrentHP;
        CurrentHP += health;

        if (CurrentHP >= MaxHP)
        {
            CurrentHP = MaxHP;
        }

        if (CurrentHP <= 0)
        {
            Died.Invoke(this);
            return;
        }

        HpChanged.Invoke(this, prev);
    }
}
