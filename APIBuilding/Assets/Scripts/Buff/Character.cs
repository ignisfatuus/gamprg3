﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public string ID;
    public Stats stats;

    // Start is called before the first frame update
    void Start()
    {
        Health health = GetComponent<Health>();
        health.Died.AddListener(OnDeath);
    }

    public void OnDeath(Health health)
    {
        Debug.Log("OnDeath");
    }

    [System.Serializable]
    public struct Stats
    {
        public int Power;
        public int Agility;
        public int Dexterity;
        public int Vitality;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
