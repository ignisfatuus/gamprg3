﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffReceiver : MonoBehaviour
{
    private List<Buff> buffs = new List<Buff>();
    public IEnumerable<Buff> Buffs { get { return buffs; } }

    public void ApplyBuff(Buff buff)
    {
        buffs.Add(buff);
        buff.Activate(this);
    }
}
