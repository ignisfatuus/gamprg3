﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vigor : MonoBehaviour
{
    public CharacterStats PlayerStats;
    public float VitalityPercentageMultipler = 50;
    public float Duration = 15;
    // Start is called before the first frame update
    void Start()
    {

        VitalityPercentageMultipler /= 100;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Effect(float quantity)
    {
        StartCoroutine(StartBuff(quantity));
        
    }

    IEnumerator StartBuff(float quantity)
    {
        PlayerStats.Vitality += (PlayerStats.Vitality * VitalityPercentageMultipler);
        yield return new WaitForSeconds(15 * quantity);
        PlayerStats.Vitality = 100;
    }

    
}
