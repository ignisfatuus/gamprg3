﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Might : MonoBehaviour
{
    public CharacterStats PlayerStats;
    public float PowToAdd=100;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public void Effect(float quantity)
    {
        PlayerStats.Power = PowToAdd;
        PowToAdd  = 100 * quantity;
   
    }
}
