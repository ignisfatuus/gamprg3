﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Regen : Buff
{
    public float HealthAmt;
    public float Interval;

    private Health health;
    private Coroutine task;
    private float duration = 1.0f;


    protected override void Update()
    {
        base.Update();

        if (IsDurInfinite)
            duration = Mathf.Infinity;
        if (Duration >= duration)
        {
            Destroy(this);
            StackCount--;
        }
    }

    public override void Activate(BuffReceiver target) // Activates the buff
    {
        base.Activate(target);
        health = target.GetComponent<Health>();

        task = StartCoroutine(RegenOverTime());
    }


    public override void Deactivate()
    {
        base.Deactivate();
        StopCoroutine(task);
    }


    IEnumerator RegenOverTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(Interval);
            if (IsStackByIntensity)
            {
                health.AddHealth(HealthAmt * StackCount);
            }

            else
            {
                health.AddHealth(HealthAmt);
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
}
