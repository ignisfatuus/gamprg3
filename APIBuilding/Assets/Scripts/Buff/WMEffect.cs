﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WMEffect : MonoBehaviour
{
    public CharacterStats PlayerStats;
    public float SelfHealPercentage = 5;
    public bool isPoisoned;
    // Start is called before the first frame update
    void Start()
    {
        SelfHealPercentage = 5 / 100;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Effect(float quantity)
    {
        int randomNumber = Random.Range(1, 100);
        if (randomNumber <= 20)
        {
            isPoisoned = false;
            PlayerStats.CurrentHealth += (PlayerStats.Health *= SelfHealPercentage);
        }
        else if (randomNumber > 20 && randomNumber <= 100)
        {
            StartCoroutine(StartBuff(quantity));
        }
        if (isPoisoned)
        {
            Debug.Log("Is Poisoned");
            PlayerStats.CurrentHealth -= (PlayerStats.Health * 0.2f);
        }
     
    }
    IEnumerator StartBuff(float quantity)
    {


        isPoisoned = true;
        yield return new WaitForSeconds(5*quantity);
        isPoisoned = false;
    }
}
