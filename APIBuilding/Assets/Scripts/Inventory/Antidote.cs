﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antidote : Item
{

    public AntidoteEffect ADEffect;
    protected override void Start()
    {
        MaxQuantity = 1;
        CurrentQuantity = 1;
        IsUsable = true;
    }



    // Update is called once per frame
    void Update()
    {
        
    }
    public override void ItemEffect()
    {
        ADEffect.Effect();
        Debug.Log("Antidote Effect");
    }
}
