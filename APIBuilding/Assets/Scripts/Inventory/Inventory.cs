﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Inventory : MonoBehaviour
{

    


    public List<Item> Items = new List<Item>();

    public bool IsFull { get; private set; }

    [SerializeField] public int MaxSlotCount;




    void Start()
    {
        ItemHolder InventoryOwner = GetComponent<ItemHolder>();

        InventoryOwner.OnItemUse.AddListener(OnItemUse);
        InventoryOwner.OnItemGain.AddListener(ActivateUnusableItemEffect);
        InventoryOwner.OnItemGain.AddListener(AddItem);
    }
    
    void Update()
    {
      
    }

    public void AddItem(Item item) 
    {
        CheckNumberOfSlots();
        if (IsFull) return;

        if (Items.Contains(item))
        {
            if (item.CurrentQuantity < item.MaxQuantity)
            {

                Debug.Log("Added quantity to item");
                item.CurrentQuantity += 1;
                item.CurrentCount += 1;
                Debug.Log("Items: " + item.CurrentQuantity);
            }
            if (item.CurrentQuantity >= item.MaxQuantity)
            {
      
                item.CurrentQuantity = 0;
                AddToNewSlot(item);
            }
        }
        else
        {
            item.CurrentCount += 1;
            AddToNewSlot(item);
        }
    }

    public void DiscardItem(Item item) 
    {
        if (!Items.Contains(item)) return;
        if (!item.IsUsable) return;
        if (item.CurrentQuantity <= 1)
        {
            
            Items.Remove(item);
            item.CurrentCount = 0;
            item.transform.parent = null;
        }
        else
        {
            item.CurrentCount -= 1;
            item.CurrentQuantity -= 1;
            Debug.Log("Items: " + item.CurrentQuantity);
        }
    }

    public  Item GetItem<T>() where T: Item
    {
        if (Items.Count < 0) return null;
        for ( int x=0;x<Items.Count;x++)
        {
            if (Items[x].GetType() == typeof(T))
            {
                return Items[x];
            }
   
        }
        return null;
    }

    public float GetQuantity<T>() where T: Item
    {

        if (Items.Count < 0) return 0;
        for (int x = 0; x < Items.Count; x++)
        {
            if (Items[x].GetType() == typeof(T))
            {
                return Items[x].CurrentCount;
       
            }


        }
        return 0;
      
    }

    private void OnItemUse(Item item)
    {
        // Press E to use Item
        if (!Items.Contains(item)) return;
        if (!item.IsUsable) return;
        
        
         item.ItemEffect();
         DiscardItem( item);

            

    }
    private void ActivateUnusableItemEffect(Item item)
    {
        // Automatically use unusable item
        if (Items.Count <=0) return;
        for (int x=0;x<Items.Count;x++)
        {
            if (Items[x].IsUsable) return;
            Items[x].ItemEffect();
        }
   

 
    }
    private void CheckNumberOfSlots()
    {
        if (Items.Count >= MaxSlotCount) IsFull = true;
        else IsFull = false;
   
    }


    private void AddToNewSlot(Item item)
    {
        item.transform.SetParent(this.transform);
        Items.Add(item);
    }





}
