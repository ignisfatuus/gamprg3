﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnItemGain : UnityEvent<Item> { }
[System.Serializable]
public class OnItemUse : UnityEvent<Item> { }

public class ItemHolder : MonoBehaviour
{
    //    public OnItemDiscarded OnItemDiscarded;
    public OnItemUse OnItemUse ;
    public OnItemGain OnItemGain;

    public Inventory MyInventory;

    public Item ItemToAdd;
    public Item ItemToUse;
    // Start is called before the first frame update
    void Start()
    {


      
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
           GainItem(ItemToAdd);
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            UseItem(ItemToUse);
        }
        
    }

   public void GainItem(Item item)
    {
        OnItemGain.Invoke(item);
    }

    public void UseItem(Item item)
    {
        OnItemUse.Invoke(item);
    }
}
