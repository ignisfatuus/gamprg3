﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RejuvenationPotion : Item
{
    public Vigor VigorBuff;
    // Start is called before the first frame update
    protected override void Start()
    {
        CurrentQuantity = 1;
        MaxQuantity = 10;
        IsUsable = true;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void ItemEffect()
    {
        VigorBuff.Effect(CurrentCount);
    }
}
