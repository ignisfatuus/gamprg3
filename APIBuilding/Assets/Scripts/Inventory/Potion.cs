﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : Item
{
    // Start is called before the first frame update
    protected override void Start()
    {
        MaxQuantity = 12;
        CurrentQuantity = 1;
        IsUsable = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void ItemEffect()
    {
        Debug.Log("Potion Effect");
    }


}
