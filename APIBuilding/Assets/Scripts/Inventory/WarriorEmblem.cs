﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorEmblem : Item
{
    public Might MightBuff;
    // Start is called before the first frame update
    protected override void Start()
    {
        CurrentCount = 1;
        CurrentQuantity = 1;
        MaxQuantity = 10;
        IsStackable = true;
        IsUsable = false;
        MightBuff = GetComponent<Might>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void ItemEffect()
    {
        MightBuff.Effect(CurrentCount);
        Debug.Log("Warrior Emblem Effect");
    }
    
}
