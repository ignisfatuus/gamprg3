﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnStore : UnityEvent { }
public class OnMaxQuantity : UnityEvent { }


public abstract class Item : MonoBehaviour
{
    // Lists


    //Bools
    public bool IsStackable { get; protected set; }
    public bool IsUsable{ get; protected set; }
    public bool IsOnMaxQuantity { get; set; }
    
    //Floats
    public float MaxQuantity { get;  protected set; }
    public float CurrentQuantity { get;  set; }
    public float CurrentCount { get; set; }

    //Events
    public OnStore OnGain;
    public OnMaxQuantity OnMaxQuantity;


    protected abstract void Start();
  
    private void CapQuantity()
    {
        if (CurrentQuantity >= MaxQuantity) CurrentQuantity = MaxQuantity;
        if (CurrentQuantity < 0) CurrentQuantity = 0;
    }

    public abstract void ItemEffect();
}
