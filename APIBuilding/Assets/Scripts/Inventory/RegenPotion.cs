﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegenPotion : Item
{

    public List<GameObject> Buffs;

    //Buffs
    public BuffReceiver Receiver;
    public Buff BuffPrefab;
    public Health health;
    public float Healthheal { get; private set; }

    // Start is called before the first frame update
    protected override void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void ItemEffect()
    {
        Debug.Log("Healing");

        Buff buff = Instantiate(BuffPrefab);
        buff.StackCount += 1;
        Healthheal += 1;
        if (buff.StackCount >= 1)
        {
            buff.Duration += 10;
            Receiver.ApplyBuff(BuffPrefab);
            health.AddHealth(Healthheal++);
        }

        if (Buffs.Count >= 1)
        {
            buff.Duration = 5;
        }
    }
}
