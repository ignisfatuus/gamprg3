﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeirdMushroom : Item
{
    public WMEffect WeirdMushroomEffect;

    // Start is called before the first frame update
    protected override void Start()
    {
        MaxQuantity = 5;
        CurrentQuantity = 1;
        IsUsable = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void ItemEffect()
    {
        WeirdMushroomEffect.Effect(CurrentCount);
        Debug.Log("Weird Mushroom Effect");
    }
}
