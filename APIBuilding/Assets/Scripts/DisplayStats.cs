﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayStats : MonoBehaviour
{
    public CharacterStats CharacterStatsComponent;
    public Text PowerDisplay;
    public Text PrecisionDisplay;
    public Text ToughnessDisplay;
    public Text Vitality;
    public Text Health;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        DisplayText();   
    }

   void DisplayText()
    {
        Health.text = CharacterStatsComponent.CurrentHealth + "/" + CharacterStatsComponent.Health;
        PowerDisplay.text = "Power:" + CharacterStatsComponent.Power.ToString();
        PrecisionDisplay.text = "Precision:" + CharacterStatsComponent.Precision.ToString();
        ToughnessDisplay.text = "Toughness:" + CharacterStatsComponent.Toughness.ToString();
        Vitality.text = "Vitality:" + CharacterStatsComponent.Vitality.ToString();
    }
}
