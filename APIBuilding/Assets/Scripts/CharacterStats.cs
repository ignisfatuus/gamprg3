﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public float CurrentHealth;
    public float Health = 100;
    public float Power = 100;
    public float Precision = 100;
    public float Toughness = 100;
    public float Vitality = 100;
    // Start is called before the first frame update
    void Start()
    {
          Health = 100;
     Power = 100;
     Precision = 100;
     Toughness = 100;
     Vitality = 100;
        CurrentHealth = 50;
}

    // Update is called once per frame
    void Update()
    {
        
    }
}
